defmodule AoC2018.Day1 do
  def part_1(data) do
    input(data) |> Enum.sum
  end

  def part_2(data) do
    input(data)
    |> Stream.cycle()
    |> Enum.reduce_while({0, MapSet.new([0])}, fn i, {current, seen} ->
      frequency = current + i

      if MapSet.member?(seen, frequency) do
        {:halt, frequency}
      else
        {:cont, {frequency, MapSet.put(seen, frequency)}}
      end
    end)
  end

  def input(body) do
    String.split(body, "\n") |> Enum.map(&String.to_integer/1)
  end
end
