defmodule AoC2018.Day3 do
  def part_1(data) do
    input(data)
    |> Enum.map(&parse_claim/1)
    |> Enum.reduce(%{}, &claim_to_map/2)
    |> Enum.count(fn {_, {v, _}} -> v > 1 end)
  end

  def parse_claim(claim) do
    r = Regex.run(~r/#([\d]+)\s\@\s([\d]+),([\d]+):\s([\d]+)x([\d]+)/, claim)
    |> Enum.slice(1..-1)
    |> Enum.map(&String.to_integer/1)

    %{id: Enum.at(r, 0), x: Enum.at(r, 1), y: Enum.at(r, 2), dx: Enum.at(r, 3), dy: Enum.at(r, 4)}
  end

  def claim_to_map(claim, claims) do
    coords = for dx <- Range.new(0, claim.dx - 1),
                 dy <- Range.new(0, claim.dy - 1), do: {dx, dy}

    Enum.reduce(coords, claims, fn {dx, dy}, acc ->
      Map.update(
        acc,
        {claim.x + dx, claim.y + dy},
        {1, [claim.id]},
        fn {count, claims} ->
          {count + 1, [claim.id | claims]}
        end)
    end)
  end

  def part_2(data) do
    parsed =
      input(data)
      |> Enum.map(&parse_claim/1)

    plot = Enum.reduce(parsed, %{}, &claim_to_map/2)

    parsed
    |> Enum.filter(fn claim -> alone?(claim, plot) end)
    |> Enum.map(fn claim -> claim.id end)
    |> Enum.at(0)
  end

  def alone?(claim, plot) do
    Enum.all?(plot, fn {_coord, {_count, claims}} ->
      not Enum.member?(claims, claim.id) || claims == [claim.id]
    end)
  end

  def input(body) do
    body
    |> String.split("\n", trim: true)
    |> Enum.map(&String.trim/1)
  end
end
