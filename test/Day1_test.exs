defmodule AoC2018Day1Test do
  use ExUnit.Case, async: true
  require Logger

  @tag :day01
  test "part 1.1 executes" do
    result = AoC2018.TestHelper.load("day1")
      |> AoC2018.Day1.part_1

    Logger.info "01.1: #{result}"
  end

  @tag :day01
  test "part 1.2 executes" do
    result = AoC2018.TestHelper.load("day1")
      |> AoC2018.Day1.part_2

      Logger.info "01.2: #{result}"
  end
end
