defmodule AoC2018Day2Test do
  use ExUnit.Case, async: true
  require Logger

  @tag :day02
  test "part 2.1 executes" do
    result = AoC2018.TestHelper.load("day2")
      |> AoC2018.Day2.part_1

    Logger.info "02.1: #{result}"
  end

  @tag :day02
  test "part 2.2 executes" do
    result = AoC2018.TestHelper.load("day2")
      |> AoC2018.Day2.part_2

      Logger.info "02.2: #{result}"
  end
end
