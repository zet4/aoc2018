defmodule AoC2018Day3Test do
  use ExUnit.Case, async: true
  require Logger

  @tag :day03
  test "part 3.1 executes with example data" do
    result = """
    #1 @ 1,3: 4x4
    #2 @ 3,1: 4x4
    #3 @ 5,5: 2x2
    """
    |> AoC2018.Day3.part_1

    assert result == 4
  end

  @tag :day03
  test "part 3.1 executes" do
    result = AoC2018.TestHelper.load("day3")
      |> AoC2018.Day3.part_1

    Logger.info "03.1: #{result}"
  end

  @tag :day03
  test "part 3.2 executes" do
    result = AoC2018.TestHelper.load("day3")
      |> AoC2018.Day3.part_2

      Logger.info "03.2: #{result}"
  end
end
