defmodule AoC2018.TestHelper do
  def load(name) do
    case File.read("test/inputs/#{name}") do
      {:ok, body}      -> body
      {:error, reason} -> raise("failed to read input: #{reason}")
    end
  end
end

ExUnit.start()
